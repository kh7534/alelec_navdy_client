package com.navdy.service.library.events;

//import android.support.v4.media.TransportMediator;
import android.view.KeyEvent;

import com.navdy.service.library.events.NavdyEvent.MessageType;
import com.squareup.wire.WireType;

public class WireUtil {
    private static final MessageType[] sMessageTypes = MessageType.values();

    public static int getEventTypeIndex(byte[] eventData) {
        return parseEventType(eventData);
    }

    public static MessageType getEventType(byte[] eventData) {
        int eventType = parseEventType(eventData);
        if (eventType <= 0 || eventType > sMessageTypes.length) {
            return null;
        }
        return sMessageTypes[eventType - 1];
    }

    public static int parseEventType(byte[] eventData) {
        int firstByte = eventData[0];
        int varType = firstByte & 7;
        if (WireType.VARINT.value() != varType) {
            throw new RuntimeException("expecting varint:" + varType);
        }
        int tag = firstByte >> 3;
        if (tag == 2) {
            return readVarint32(eventData);
        }
        throw new RuntimeException("unexpected tag:" + tag);
    }

    public static int readVarint32(byte[] data) {
        byte tmp = data[1];
        if (tmp >= (byte) 0) {
            return tmp;
        }
        int result = tmp & KeyEvent.KEYCODE_MEDIA_PAUSE;
        int pos = 1 + 1;
        tmp = data[pos];
        if (tmp >= (byte) 0) {
            return result | (tmp << 7);
        }
        result |= (tmp & KeyEvent.KEYCODE_MEDIA_PAUSE) << 7;
        pos++;
        tmp = data[pos];
        if (tmp >= (byte) 0) {
            return result | (tmp << 14);
        }
        result |= (tmp & KeyEvent.KEYCODE_MEDIA_PAUSE) << 14;
        pos++;
        tmp = data[pos];
        if (tmp >= (byte) 0) {
            return result | (tmp << 21);
        }
        result |= (tmp & KeyEvent.KEYCODE_MEDIA_PAUSE) << 21;
        pos++;
        tmp = data[pos];
        result |= tmp << 28;
        if (tmp >= (byte) 0) {
            return result;
        }
        for (int i = 0; i < 5; i++) {
            pos++;
            if (data[pos] >= (byte) 0) {
                return result;
            }
        }
        throw new RuntimeException("marlformed varint");
    }
}
