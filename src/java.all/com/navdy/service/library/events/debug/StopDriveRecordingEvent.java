package com.navdy.service.library.events.debug;

import com.squareup.wire.Message;

public final class StopDriveRecordingEvent extends Message {
    private static final long serialVersionUID = 0;

    public static final class Builder extends com.squareup.wire.Message.Builder<StopDriveRecordingEvent> {
        public Builder(StopDriveRecordingEvent message) {
            super(message);
        }

        public StopDriveRecordingEvent build() {
            return new StopDriveRecordingEvent(this);
        }
    }

    private StopDriveRecordingEvent(Builder builder) {
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        return other instanceof StopDriveRecordingEvent;
    }

    public int hashCode() {
        return 0;
    }
}
