package com.navdy.service.library.events.callcontrol;

import com.squareup.wire.Message;
import com.squareup.wire.Message.Datatype;
import com.squareup.wire.Message.Label;
import com.squareup.wire.ProtoEnum;
import com.squareup.wire.ProtoField;

public final class PhoneBatteryStatus extends Message {
    public static final Boolean DEFAULT_CHARGING = Boolean.valueOf(false);
    public static final Integer DEFAULT_LEVEL = Integer.valueOf(0);
    public static final BatteryStatus DEFAULT_STATUS = BatteryStatus.BATTERY_OK;
    private static final long serialVersionUID = 0;
    @ProtoField(tag = 3, type = Datatype.BOOL)
    public final Boolean charging;
    @ProtoField(tag = 2, type = Datatype.INT32)
    public final Integer level;
    @ProtoField(label = Label.REQUIRED, tag = 1, type = Datatype.ENUM)
    public final BatteryStatus status;

    public enum BatteryStatus implements ProtoEnum {
        BATTERY_OK(1),
        BATTERY_LOW(2),
        BATTERY_EXTREMELY_LOW(3);
        
        private final int value;

        private BatteryStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static final class Builder extends com.squareup.wire.Message.Builder<PhoneBatteryStatus> {
        public Boolean charging;
        public Integer level;
        public BatteryStatus status;

        public Builder(PhoneBatteryStatus message) {
            super(message);
            if (message != null) {
                this.status = message.status;
                this.level = message.level;
                this.charging = message.charging;
            }
        }

        public Builder status(BatteryStatus status) {
            this.status = status;
            return this;
        }

        public Builder level(Integer level) {
            this.level = level;
            return this;
        }

        public Builder charging(Boolean charging) {
            this.charging = charging;
            return this;
        }

        public PhoneBatteryStatus build() {
            checkRequiredFields();
            return new PhoneBatteryStatus(this);
        }
    }

    public PhoneBatteryStatus(BatteryStatus status, Integer level, Boolean charging) {
        this.status = status;
        this.level = level;
        this.charging = charging;
    }

    private PhoneBatteryStatus(Builder builder) {
        this(builder.status, builder.level, builder.charging);
        setBuilder(builder);
    }

    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof PhoneBatteryStatus)) {
            return false;
        }
        PhoneBatteryStatus o = (PhoneBatteryStatus) other;
        if (equals((Object) this.status, (Object) o.status) && equals((Object) this.level, (Object) o.level) && equals((Object) this.charging, (Object) o.charging)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int result = this.hashCode;
        if (result != 0) {
            return result;
        }
        int hashCode;
        int hashCode2 = (this.status != null ? this.status.hashCode() : 0) * 37;
        if (this.level != null) {
            hashCode = this.level.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (hashCode2 + hashCode) * 37;
        if (this.charging != null) {
            i = this.charging.hashCode();
        }
        result = hashCode + i;
        this.hashCode = result;
        return result;
    }
}
