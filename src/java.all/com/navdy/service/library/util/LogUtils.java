package com.navdy.service.library.util;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Debug;
import android.os.Environment;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.navdy.service.library.log.FastPrintWriter;
import com.navdy.service.library.log.Logger;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class LogUtils {
    private static final int BUFFER_SIZE = 32768;
    private static final String CRASH_DUMP_FILE = "crash.log";
    private static final String CRASH_DUMP_STAGING = "staging";
    private static final byte[] CRLF = "\r\n".getBytes();
    private static RotatingLogFile[] LOG_FILES = new RotatingLogFile[]{new RotatingLogFile("a.log", 4), new RotatingLogFile("obd.log", 2), new RotatingLogFile("mfi.log", 2), new RotatingLogFile("lighting.log", 2), new RotatingLogFile("dial.log", 4), new RotatingLogFile("obdRaw.log", 2)};
    private static RotatingLogFile[] LOG_FILES_FOR_SNAPSHOT = new RotatingLogFile[]{new RotatingLogFile("a.log", 2), new RotatingLogFile("obd.log", 2), new RotatingLogFile("mfi.log", 2), new RotatingLogFile("lighting.log", 2), new RotatingLogFile("dial.log", 2), new RotatingLogFile("obdRaw.log", 2)};
    private static final String LOG_MARKER = "--------- beginning of main";
    private static final String SYSTEM_LOG_PATH = ".logs";
    private static final String TAG = LogUtils.class.getName();
    private static Logger sLogger = new Logger(LogUtils.class);

    private static class RotatingLogFile {
        int maxFiles;
        String name;

        RotatingLogFile(String name, int maxFiles) {
            this.name = name;
            this.maxFiles = maxFiles;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072 A:{SYNTHETIC, Splitter: B:21:0x0072} */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072 A:{SYNTHETIC, Splitter: B:21:0x0072} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void dumpLog(Context context, String fileName) throws Throwable {
        Throwable th;
        synchronized (LogUtils.class) {
            if (new File(fileName).exists()) {
                IOUtils.deleteFile(context, fileName);
            }
            Process process = null;
            BufferedOutputStream bufferedOutputStream = null;
            BufferedInputStream bufferedInputStream = null;
            InputStream processInputStream = null;
            byte[] buffer = new byte[32768];
            FileOutputStream fileOutputStream = new FileOutputStream(fileName);
            try {
                BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(fileOutputStream, 32768);
                try {
                    process = Runtime.getRuntime().exec(new String[]{"logcat", "-d", "-v", "time"});
                    processInputStream = process.getInputStream();
                    BufferedInputStream bufferedInputStream2 = new BufferedInputStream(processInputStream);
                    while (true) {
                        try {
                            int len = bufferedInputStream2.read(buffer);
                            if (len <= 0) {
                                break;
                            }
                            bufferedOutputStream2.write(buffer, 0, len);
                        } catch (Throwable th2) {
                            th = th2;
                            bufferedInputStream = bufferedInputStream2;
                            bufferedOutputStream = bufferedOutputStream2;
                            IOUtils.fileSync(fileOutputStream);
                            IOUtils.closeStream(bufferedOutputStream);
                            IOUtils.closeStream(bufferedInputStream);
                            IOUtils.closeStream(fileOutputStream);
                            IOUtils.closeStream(processInputStream);
                            if (process != null) {
                            }
                            throw th;
                        }
                    }
                    bufferedOutputStream2.flush();
                    IOUtils.fileSync(fileOutputStream);
                    IOUtils.closeStream(bufferedOutputStream2);
                    IOUtils.closeStream(bufferedInputStream2);
                    IOUtils.closeStream(fileOutputStream);
                    IOUtils.closeStream(processInputStream);
                    if (process != null) {
                        try {
                            process.destroy();
                        } catch (Throwable e) {
                            sLogger.e(e);
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    bufferedOutputStream = bufferedOutputStream2;
                    IOUtils.fileSync(fileOutputStream);
                    IOUtils.closeStream(bufferedOutputStream);
                    IOUtils.closeStream(bufferedInputStream);
                    IOUtils.closeStream(fileOutputStream);
                    IOUtils.closeStream(processInputStream);
                    if (process != null) {
                        try {
                            process.destroy();
                        } catch (Throwable e2) {
                            sLogger.e(e2);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                IOUtils.fileSync(fileOutputStream);
                IOUtils.closeStream(bufferedOutputStream);
                IOUtils.closeStream(bufferedInputStream);
                IOUtils.closeStream(fileOutputStream);
                IOUtils.closeStream(processInputStream);
                if (process != null) {
                }
                throw th;
            }
        }
        return;
    }

    public static synchronized void createCrashDump(Context context, String path, Thread crashThread, Throwable throwable, boolean nativeCrash) {
        Throwable t;
        Throwable th;
        synchronized (LogUtils.class) {
            String processName;
            String stagingPath;
            FileOutputStream crashInfo = null;
            try {
                processName = SystemUtils.getProcessName(context, Process.myPid());
                Log.v(TAG, "creating CrashDump");
                File stagingDir = new File(path + File.separator + CRASH_DUMP_STAGING);
                if (stagingDir.exists()) {
                    IOUtils.deleteDirectory(context, stagingDir);
                }
                IOUtils.createDirectory(stagingDir);
                stagingPath = stagingDir.getAbsolutePath();
                FileOutputStream crashInfo2 = new FileOutputStream(stagingPath + File.separator + CRASH_DUMP_FILE);
                try {
                    writeCrashInfo(crashInfo2, throwable, processName, crashThread, nativeCrash);
                    IOUtils.fileSync(crashInfo2);
                    IOUtils.closeStream(crashInfo2);
                    crashInfo = null;
                    copyComprehensiveSystemLogs(stagingPath);
                } catch (Throwable th2) {
                    th = th2;
                    crashInfo = crashInfo2;
                    IOUtils.closeStream(crashInfo);
                    throw th;
                }
            } catch (Throwable th3) {
                t = th3;
            }
            String tag = "";
            if (nativeCrash) {
                tag = "_native";
            }
            zipStaging(context, path + File.separator + processName + tag + "_crash_" + System.currentTimeMillis() + ".zip", stagingPath);
            IOUtils.deleteDirectory(context, new File(stagingPath));
            IOUtils.closeStream(null);
        }
    }

    private static void writeCrashInfo(FileOutputStream fileOutputStream, Throwable throwable, String processName, Thread crashThread, boolean nativeCrash) throws Throwable {
        Runtime runtime = Runtime.getRuntime();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        bufferedOutputStream.write(("process: " + processName).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("time: " + Calendar.getInstance().getTime().toString()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("serial: " + Build.SERIAL).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("device: " + Build.DEVICE).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("model: " + Build.MODEL).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("hw: " + Build.HARDWARE).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("id: " + Build.ID).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("version: " + VERSION.SDK_INT).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("bootloader: " + Build.BOOTLOADER).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("board: " + Build.BOARD).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("brand: " + Build.BRAND).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("manufacturer: " + Build.MANUFACTURER).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("cpu_abi: " + Build.CPU_ABI).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("cpu_abi2: " + Build.CPU_ABI2).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("processors: " + runtime.availableProcessors()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        if (nativeCrash) {
            bufferedOutputStream.write("------ native crash -----".getBytes());
        } else {
            bufferedOutputStream.write("------ crash -----".getBytes());
        }
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("thread name crashed:" + crashThread.getName()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        Writer sw = new StringWriter();
        PrintWriter pw = new FastPrintWriter(sw, false, 256);
        throwable.printStackTrace(pw);
        pw.flush();
        bufferedOutputStream.write(sw.toString().getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write("------ state -----".getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("total:" + runtime.totalMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("free:" + runtime.freeMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("max:" + runtime.maxMemory()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap size: " + Debug.getNativeHeapSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap allocated: " + Debug.getNativeHeapAllocatedSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(("native heap free: " + Debug.getNativeHeapFreeSize()).getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        Map<Thread, StackTraceElement[]> stackTraces = Thread.getAllStackTraces();
        bufferedOutputStream.write(("------ thread stackstraces count = " + stackTraces.size() + " -----").getBytes());
        bufferedOutputStream.write(CRLF);
        bufferedOutputStream.write(CRLF);
        for (Entry<Thread, StackTraceElement[]> entry : stackTraces.entrySet()) {
            Thread t = (Thread) entry.getKey();
            bufferedOutputStream.write(("\"" + t.getName() + "\"").getBytes());
            bufferedOutputStream.write(CRLF);
            bufferedOutputStream.write(("    Thread.State: " + t.getState()).getBytes());
            bufferedOutputStream.write(CRLF);
            for (StackTraceElement element : (StackTraceElement[]) entry.getValue()) {
                bufferedOutputStream.write(("       at " + element).getBytes());
                bufferedOutputStream.write(CRLF);
            }
            bufferedOutputStream.write(CRLF);
            bufferedOutputStream.write(CRLF);
        }
        bufferedOutputStream.flush();
    }

    private static String getSystemLog() {
        String contents = null;
        try {
            return IOUtils.convertFileToString((Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + SYSTEM_LOG_PATH) + File.separator + "a.log");
        } catch (IOException e) {
            Log.i(TAG, "Failed to grab system log");
            return contents;
        }
    }

    public static String systemLogStr(int bytes, String str) {
        String contents = getSystemLog();
        if (contents == null) {
            return contents;
        }
        int llen = contents.length();
        int end = llen;
        if (str != null) {
            int slen = str.length();
            int strIdx = contents.lastIndexOf(str);
            if (strIdx != -1) {
                end = Math.min((strIdx + slen) + 10, llen);
            }
        }
        return contents.substring(Math.max(end - bytes, 0), end);
    }

    public static void copyComprehensiveSystemLogs(String stagingPath) throws Throwable {
        copySystemLogs(stagingPath, LOG_FILES);
    }

    public static void copySnapshotSystemLogs(String stagingPath) throws Throwable {
        copySystemLogs(stagingPath, LOG_FILES_FOR_SNAPSHOT);
    }

    public static void copySystemLogs(String stagingPath, RotatingLogFile[] logFilesRequirement) throws Throwable {
        String srcPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + SYSTEM_LOG_PATH;
        for (RotatingLogFile logFile : logFilesRequirement) {
            String filename = logFile.name;
            int count = logFile.maxFiles;
            for (int i = 0; i < count; i++) {
                String logFileName = filename;
                if (i > 0) {
                    logFileName = logFileName + "." + i;
                }
                IOUtils.copyFile(srcPath + File.separator + logFileName, stagingPath + File.separator + logFileName);
            }
        }
    }

    public static void zipStaging(Context context, String filePath, String stagingPath) throws Throwable {
        Throwable t;
        Throwable th;
        FileInputStream fis;
        Object fis2;
        Log.i(TAG, "zip started:" + filePath);
        FileOutputStream fos = null;
        Closeable fis3 = null;
        ZipOutputStream zos = null;
        try {
            ZipOutputStream zipOutputStream;
            long l1 = SystemClock.elapsedRealtime();
            byte[] buffer = new byte[16384];
            FileOutputStream fos2 = new FileOutputStream(filePath);
            try {
                zipOutputStream = new ZipOutputStream(fos2);
            } catch (Throwable th2) {
                th = th2;
                fos = fos2;
                IOUtils.closeStream(zos);
                IOUtils.closeStream(fis3);
                IOUtils.closeStream(fos);
                throw th;
            }
            try {
                File[] files = new File(stagingPath).listFiles();
                if (files != null) {
                    int i = 0;
                    while (true) {
                        try {
                            FileInputStream fis4;
                            fis = fis4;
                            if (i >= files.length) {
                                break;
                            }
                            fis3 = new FileInputStream(files[i]);
                            zipOutputStream.putNextEntry(new ZipEntry(files[i].getName()));
                            while (true) {
                                int length = fis3.read(buffer);
                                if (length <= 0) {
                                    break;
                                }
                                zipOutputStream.write(buffer, 0, length);
                            }
                            zipOutputStream.closeEntry();
                            IOUtils.closeStream(fis3);
                            fis4 = null;
                            i++;
                        } catch (Throwable th3) {
                            th = th3;
                            zos = zipOutputStream;
                            fis2 = fis;
                            fos = fos2;
                            IOUtils.closeStream(zos);
                            IOUtils.closeStream(fis3);
                            IOUtils.closeStream(fos);
                            throw th;
                        }
                    }
                    fis3 = fis;
                }
                Log.i(TAG, "zip finished:" + filePath + " time:" + (SystemClock.elapsedRealtime() - l1));
                IOUtils.closeStream(zipOutputStream);
                IOUtils.closeStream(fis3);
                IOUtils.closeStream(fos2);
            } catch (Throwable th4) {
                th = th4;
                zos = zipOutputStream;
                fos = fos2;
                IOUtils.closeStream(zos);
                IOUtils.closeStream(fis3);
                IOUtils.closeStream(fos);
                throw th;
            }
        } catch (Throwable th5) {
            t = th5;
            IOUtils.closeStream(fos);
            IOUtils.deleteFile(context, filePath);
            Log.i(TAG, "zip error", t);
            throw t;
        }
    }

    public static synchronized void createKernelCrashDump(Context context, String path, String[] kernelCrashFiles) {
        synchronized (LogUtils.class) {
            try {
                File stagingDir = new File(path + File.separator + CRASH_DUMP_STAGING);
                if (stagingDir.exists()) {
                    IOUtils.deleteDirectory(context, stagingDir);
                }
                IOUtils.createDirectory(stagingDir);
                int count = 0;
                String stagingPath = stagingDir.getAbsolutePath();
                for (String str : kernelCrashFiles) {
                    if (IOUtils.copyFile(str, stagingPath + File.separator + new File(str).getName())) {
                        sLogger.v("copied:" + str);
                        count++;
                    }
                }
                if (count > 0) {
                    String fileName = path + File.separator + "kernel_crash_" + System.currentTimeMillis() + ".zip";
                    zipStaging(context, fileName, stagingPath);
                    sLogger.v("kernel dump created:" + fileName);
                } else {
                    sLogger.v("no kernel crash files");
                }
                IOUtils.deleteDirectory(context, new File(stagingPath));
            } catch (Throwable t) {
                Log.e(TAG, "createKernelCrashDump", t);
            }
        }
        return;
    }
}
