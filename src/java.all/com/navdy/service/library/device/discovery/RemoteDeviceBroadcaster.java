package com.navdy.service.library.device.discovery;

public interface RemoteDeviceBroadcaster {
    boolean start();

    boolean stop();
}
