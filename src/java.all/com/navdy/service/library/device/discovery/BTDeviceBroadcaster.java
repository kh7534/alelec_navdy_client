package com.navdy.service.library.device.discovery;

import android.bluetooth.BluetoothAdapter;
import com.navdy.service.library.log.Logger;
import java.util.Locale;
import java.util.regex.Pattern;

public class BTDeviceBroadcaster implements RemoteDeviceBroadcaster {
    public static final int DISCOVERY_TIMEOUT = 300;
    public static final String DISPLAY_NAME = " Navdy Display";
    public static final Pattern DISPLAY_PATTERN = Pattern.compile("navdy|hud");
    private static final Logger sLogger = new Logger(BTDeviceBroadcaster.class);
    private BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
    private boolean started;

    public static boolean isDisplay(String bluetoothName) {
        return DISPLAY_PATTERN.matcher(bluetoothName.toLowerCase(Locale.US)).find();
    }

    public boolean start() {
        if (!this.started) {
            this.started = true;
            setScanMode(23, 300);
        }
        return this.started;
    }

    public boolean stop() {
        if (this.started) {
            this.started = false;
            setScanMode(20, 300);
        }
        return true;
    }

    private void setScanMode(int mode, int timeout) {
        try {
            Class.forName(this.adapter.getClass().getName()).getDeclaredMethod("setScanMode", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(this.adapter, new Object[]{Integer.valueOf(mode), Integer.valueOf(timeout)});
        } catch (Exception ex) {
            sLogger.d("enableDiscovery failed - " + ex.toString(), ex);
        }
    }

    private void ensureDisplayLike() {
        String name = this.adapter.getName();
        if (!isDisplay(name)) {
            this.adapter.setName(name + DISPLAY_NAME);
        }
    }
}
