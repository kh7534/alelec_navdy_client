package com.navdy.client.ota;

import com.navdy.client.ota.model.UpdateInfo;
import com.navdy.service.library.events.file.FileTransferError;

public interface OTAUpdateListener {

    public enum CheckOTAUpdateResult {
        AVAILABLE,
        UPTODATE,
        SERVER_ERROR,
        NO_CONNECTIVITY
    }

    public enum DownloadUpdateStatus {
        STARTED,
        PAUSED,
        DOWNLOADING,
        NOT_ENOUGH_SPACE,
        NO_CONNECTIVITY,
        DOWNLOAD_FAILED,
        COMPLETED
    }

    public enum UploadToHUDStatus {
        UPLOADING,
        DEVICE_NOT_CONNECTED,
        UPLOAD_FAILED,
        COMPLETED
    }

    void onCheckForUpdateFinished(CheckOTAUpdateResult checkOTAUpdateResult, UpdateInfo updateInfo);

    void onDownloadProgress(DownloadUpdateStatus downloadUpdateStatus, int i, long j, long j2);

    void onUploadProgress(UploadToHUDStatus uploadToHUDStatus, long j, long j2, FileTransferError fileTransferError);
}
