package com.navdy.client.app.framework.callcontrol;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.view.KeyEvent;
import com.navdy.client.app.framework.service.RemoteControllerNotificationListenerService;
import com.navdy.service.library.log.Logger;
import java.util.HashSet;
import java.util.List;

public class TelephonySupport21 implements TelephonyInterface {
    static HashSet<String> TELECOM_PACKAGES = new HashSet();
    private static final Logger sLogger = new Logger(TelephonySupport21.class);
    protected Context context;

    enum Action {
        ACCEPT_CALL,
        REJECT_CALL,
        END_CALL
    }

    static {
        TELECOM_PACKAGES.add("com.android.server.telecom");
        TELECOM_PACKAGES.add("com.android.phone");
    }

    public TelephonySupport21(Context context) {
        this.context = context;
    }

    public void acceptRingingCall() {
        handleCallAction(Action.ACCEPT_CALL);
    }

    public void rejectRingingCall() {
        handleCallAction(Action.REJECT_CALL);
    }

    public void endCall() {
        handleCallAction(Action.END_CALL);
    }

    public void toggleMute() {
        sLogger.w("not implemented");
    }

    @TargetApi(21)
    protected void handleCallAction(Action action) {
        try {
            MediaSessionManager mediaSessionManager = (MediaSessionManager) this.context.getSystemService("media_session");
            if (mediaSessionManager == null) {
                sLogger.e("Unable to handleCallAction. mediaSessionManager is null");
                return;
            }
            List<MediaController> mediaControllerList = mediaSessionManager.getActiveSessions(new ComponentName(this.context, RemoteControllerNotificationListenerService.class));
            sLogger.v("mediaSessions[" + mediaControllerList.size() + "] action[" + action + "]");
            for (MediaController m : mediaControllerList) {
                String packageName = m.getPackageName();
                sLogger.v("package is " + packageName);
                if (TELECOM_PACKAGES.contains(packageName)) {
                    switch (action) {
                        case ACCEPT_CALL:
                            accept(m);
                            break;
                        case END_CALL:
                            end(m);
                            break;
                        case REJECT_CALL:
                            reject(m);
                            break;
                        default:
                            break;
                    }
                }
            }
        } catch (Throwable t) {
            sLogger.e("Unable to handleCallAction", t);
        }
    }

    @TargetApi(21)
    protected void accept(MediaController m) {
        m.dispatchMediaButtonEvent(KeyEvent.changeFlags(new KeyEvent(1, 79), 0));
        sLogger.i("HEADSETHOOK keyup sent:" + m.getPackageName());
    }

    @TargetApi(21)
    protected void reject(MediaController m) {
        m.dispatchMediaButtonEvent(KeyEvent.changeFlags(new KeyEvent(1, 79), 128));
        sLogger.i("HEADSETHOOK keyup sent:" + m.getPackageName());
    }

    protected void end(MediaController m) {
        reject(m);
    }
}
