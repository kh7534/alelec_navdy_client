package com.navdy.client.app.framework.servicehandler;

import android.content.SharedPreferences;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class SpeechServiceHandler$$InjectAdapter extends Binding<SpeechServiceHandler> implements Provider<SpeechServiceHandler>, MembersInjector<SpeechServiceHandler> {
    private Binding<TTSAudioRouter> audioRouter;
    private Binding<SharedPreferences> sharedPreferences;

    public SpeechServiceHandler$$InjectAdapter() {
        super("com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler", false, SpeechServiceHandler.class);
    }

    public void attach(Linker linker) {
        this.audioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", SpeechServiceHandler.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", SpeechServiceHandler.class, getClass().getClassLoader());
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.audioRouter);
        injectMembersBindings.add(this.sharedPreferences);
    }

    public SpeechServiceHandler get() {
        SpeechServiceHandler result = new SpeechServiceHandler();
        injectMembers(result);
        return result;
    }

    public void injectMembers(SpeechServiceHandler object) {
        object.audioRouter = (TTSAudioRouter) this.audioRouter.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
    }
}
