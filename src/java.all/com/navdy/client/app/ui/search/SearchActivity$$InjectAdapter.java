package com.navdy.client.app.ui.search;

import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.ui.base.BaseActivity;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class SearchActivity$$InjectAdapter extends Binding<SearchActivity> implements Provider<SearchActivity>, MembersInjector<SearchActivity> {
    private Binding<TTSAudioRouter> mAudioRouter;
    private Binding<BaseActivity> supertype;

    public SearchActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.search.SearchActivity", "members/com.navdy.client.app.ui.search.SearchActivity", false, SearchActivity.class);
    }

    public void attach(Linker linker) {
        this.mAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", SearchActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseActivity", SearchActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.mAudioRouter);
        injectMembersBindings.add(this.supertype);
    }

    public SearchActivity get() {
        SearchActivity result = new SearchActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(SearchActivity object) {
        object.mAudioRouter = (TTSAudioRouter) this.mAudioRouter.get();
        this.supertype.injectMembers(object);
    }
}
