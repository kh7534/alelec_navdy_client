package com.navdy.client.app.ui.settings;

import android.content.SharedPreferences;
import com.navdy.client.app.framework.util.TTSAudioRouter;
import com.navdy.client.app.ui.base.BaseEditActivity;
import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

public final class HFPAudioDelaySettingsActivity$$InjectAdapter extends Binding<HFPAudioDelaySettingsActivity> implements Provider<HFPAudioDelaySettingsActivity>, MembersInjector<HFPAudioDelaySettingsActivity> {
    private Binding<SharedPreferences> sharedPreferences;
    private Binding<BaseEditActivity> supertype;
    private Binding<TTSAudioRouter> ttsAudioRouter;

    public HFPAudioDelaySettingsActivity$$InjectAdapter() {
        super("com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", "members/com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity", false, HFPAudioDelaySettingsActivity.class);
    }

    public void attach(Linker linker) {
        this.ttsAudioRouter = linker.requestBinding("com.navdy.client.app.framework.util.TTSAudioRouter", HFPAudioDelaySettingsActivity.class, getClass().getClassLoader());
        this.sharedPreferences = linker.requestBinding("android.content.SharedPreferences", HFPAudioDelaySettingsActivity.class, getClass().getClassLoader());
        Linker linker2 = linker;
        this.supertype = linker2.requestBinding("members/com.navdy.client.app.ui.base.BaseEditActivity", HFPAudioDelaySettingsActivity.class, getClass().getClassLoader(), false, true);
    }

    public void getDependencies(Set<Binding<?>> set, Set<Binding<?>> injectMembersBindings) {
        injectMembersBindings.add(this.ttsAudioRouter);
        injectMembersBindings.add(this.sharedPreferences);
        injectMembersBindings.add(this.supertype);
    }

    public HFPAudioDelaySettingsActivity get() {
        HFPAudioDelaySettingsActivity result = new HFPAudioDelaySettingsActivity();
        injectMembers(result);
        return result;
    }

    public void injectMembers(HFPAudioDelaySettingsActivity object) {
        object.ttsAudioRouter = (TTSAudioRouter) this.ttsAudioRouter.get();
        object.sharedPreferences = (SharedPreferences) this.sharedPreferences.get();
        this.supertype.injectMembers(object);
    }
}
