package com.navdy.client.app.ui.base;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import com.navdy.client.app.framework.util.BusProvider;
import com.navdy.service.library.log.Logger;

public class BaseDialogFragment extends DialogFragment {
    protected BaseActivity baseActivity;
    protected final Handler handler = new Handler();
    protected final Logger logger = new Logger(getClass());
    private volatile boolean mIsInForeground = false;

    public void onCreate(Bundle savedInstanceState) {
        this.logger.v("::onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onPause() {
        this.logger.v("::onPause");
        super.onPause();
        this.mIsInForeground = false;
        BusProvider.getInstance().unregister(this);
    }

    public void onResume() {
        this.logger.v("::onResume");
        super.onResume();
        this.mIsInForeground = true;
        BusProvider.getInstance().register(this);
    }

    public void onSaveInstanceState(Bundle state) {
        this.mIsInForeground = false;
        super.onSaveInstanceState(state);
    }

    public boolean isInForeground() {
        return this.mIsInForeground;
    }

    public void onAttach(Activity activity) {
        this.logger.v("::onAttach");
        super.onAttach(activity);
        if (activity instanceof BaseActivity) {
            this.baseActivity = (BaseActivity) activity;
        }
    }

    public void onDetach() {
        this.logger.v("::onDetach");
        this.baseActivity = null;
        super.onDetach();
    }

    public void onDestroy() {
        this.logger.v("::onDestroy");
        super.onDestroy();
    }

    public boolean isAlive() {
        return (this.baseActivity == null || this.baseActivity.isActivityDestroyed()) ? false : true;
    }
}
