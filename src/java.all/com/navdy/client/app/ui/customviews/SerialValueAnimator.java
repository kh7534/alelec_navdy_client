package com.navdy.client.app.ui.customviews;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.view.animation.LinearInterpolator;
import java.util.LinkedList;
import java.util.Queue;

public class SerialValueAnimator {
    private DefaultAnimationListener animationListener = new DefaultAnimationListener() {
        public void onAnimationEnd(Animator animation) {
            if (SerialValueAnimator.this.mValueQueue.size() == 0) {
                SerialValueAnimator.this.mAnimationRunning = false;
                SerialValueAnimator.this.mAnimator = null;
                return;
            }
            SerialValueAnimator.this.animate(((Float) SerialValueAnimator.this.mValueQueue.remove()).floatValue());
        }
    };
    private SerialValueAnimatorAdapter mAdapter;
    private boolean mAnimationRunning;
    private ValueAnimator mAnimator;
    private int mDuration;
    private int mReverseDuration = 100;
    private Queue<Float> mValueQueue = new LinkedList();

    public interface SerialValueAnimatorAdapter {
        float getValue();

        void setValue(float f);
    }

    public SerialValueAnimator(SerialValueAnimatorAdapter adapter, int animationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        if (adapter == null) {
            throw new IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public SerialValueAnimator(SerialValueAnimatorAdapter adapter, int animationDuration, int reverseAnimationDuration) {
        this.mAdapter = adapter;
        this.mDuration = animationDuration;
        this.mReverseDuration = reverseAnimationDuration;
        if (adapter == null) {
            throw new IllegalArgumentException("SerialValueAnimator cannot run without the adapter");
        }
    }

    public void setValue(float val) {
        if (this.mAdapter.getValue() == val) {
            this.mAdapter.setValue(val);
        } else if (this.mAnimationRunning) {
            this.mValueQueue.add(Float.valueOf(val));
        } else {
            this.mAnimationRunning = true;
            animate(val);
        }
    }

    public void setAnimationSpeeds(int forwardAnimationDuration, int reverseAnimationDuration) {
        this.mDuration = forwardAnimationDuration;
        this.mReverseDuration = reverseAnimationDuration;
    }

    private void animate(float value) {
        this.mAnimator = ValueAnimator.ofFloat(new float[]{this.mAdapter.getValue(), value});
        if (this.mAdapter.getValue() > value) {
            this.mAnimator.setDuration((long) this.mReverseDuration);
        } else {
            this.mAnimator.setDuration((long) this.mDuration);
        }
        this.mAnimator.setInterpolator(new LinearInterpolator());
        this.mAnimator.addUpdateListener(new AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                SerialValueAnimator.this.mAdapter.setValue(((Float) animation.getAnimatedValue()).floatValue());
            }
        });
        this.mAnimator.addListener(this.animationListener);
        this.mAnimator.start();
    }

    public void release() {
        this.mValueQueue.clear();
        if (this.mAnimator != null) {
            this.mAnimator.removeAllListeners();
            this.mAnimator.removeAllUpdateListeners();
            this.mAnimator.cancel();
        }
        this.mAnimator = null;
        this.mAnimationRunning = false;
    }
}
