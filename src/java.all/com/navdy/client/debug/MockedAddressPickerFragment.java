package com.navdy.client.debug;

import android.os.Bundle;
import com.alelec.navdyclient.R;
import com.navdy.client.app.framework.AppInstance;
import com.navdy.service.library.events.location.Coordinate;

public class MockedAddressPickerFragment extends RemoteAddressPickerFragment {
    private AppInstance appInstance;

    static {
        fragmentTitle = R.string.title_mocked_position_search;
    }

    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        this.appInstance = AppInstance.getInstance();
    }

    protected void processSelectedLocation(Coordinate location, String locationLabel, String streetAddress) {
        this.appInstance.showToast("Current location mocked to " + locationLabel, false);
        getFragmentManager().popBackStack();
    }
}
