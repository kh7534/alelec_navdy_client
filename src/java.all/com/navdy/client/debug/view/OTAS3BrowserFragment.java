package com.navdy.client.debug.view;

import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.navdy.client.debug.file.FileTransferManager.FileTransferListener;
import com.navdy.client.debug.file.RemoteFileTransferManager;
import com.navdy.client.debug.util.FormatUtils;
import com.navdy.client.debug.util.S3Constants;
import com.navdy.service.library.events.file.FileTransferError;
import com.navdy.service.library.events.file.FileTransferResponse;
import com.navdy.service.library.events.file.FileTransferStatus;
import com.navdy.service.library.events.file.FileType;
import com.navdy.service.library.log.Logger;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class OTAS3BrowserFragment extends S3BrowserFragment {
    private static final Logger sLogger = new Logger(VideoS3BrowserFragment.class);

    private class TempFileTransferListener implements FileTransferListener {
        private static final int REPORT_ON_CHUNKS_STEP = 5;
        long fileSize;
        String fileSizeString = FormatUtils.readableFileSize(this.fileSize);
        long previousThroughPutReportSize;
        long previousThroughPutReportTime;
        long startTime;
        File transferredFile;

        TempFileTransferListener(File transferredFile) {
            this.transferredFile = transferredFile;
            this.fileSize = transferredFile.length();
        }

        public void onError(FileTransferError errorCode, String error) {
            if (!this.transferredFile.delete()) {
                OTAS3BrowserFragment.sLogger.e("Unable to delete transferred file: " + this.transferredFile);
            }
            OTAS3BrowserFragment.sLogger.e(error);
            OTAS3BrowserFragment.this.mAppInstance.showToast("Error while sending file. Check log.", false);
        }

        private void showMessageAndDeleteFile(String msg) {
            if (!this.transferredFile.delete()) {
                OTAS3BrowserFragment.sLogger.e("Unable to delete transferred file: " + this.transferredFile);
            }
            OTAS3BrowserFragment.this.mAppInstance.showToast(msg, false);
        }

        public void onFileTransferResponse(FileTransferResponse fileTransferResponse) {
            OTAS3BrowserFragment.sLogger.i(String.format("onFileTransferResponse: %s transferId: %s", new Object[]{fileTransferResponse, fileTransferResponse.transferId}));
            if (fileTransferResponse.success.booleanValue()) {
                OTAS3BrowserFragment.this.mAppInstance.showToast(String.format("File transfer (%s) request acknowledged", new Object[]{fileTransferResponse.transferId}), false, 0);
                this.startTime = System.currentTimeMillis() / 1000;
                this.previousThroughPutReportTime = this.startTime;
                this.previousThroughPutReportSize = 0;
                return;
            }
            showMessageAndDeleteFile(String.format("File transfer (%s) request declined with status %s", new Object[]{fileTransferResponse.transferId, fileTransferResponse.error}));
        }

        public void onFileTransferStatus(FileTransferStatus fileTransferStatus) {
            OTAS3BrowserFragment.sLogger.i(String.format("onFileTransferStatus: %s transferId: %s chunk: %s", new Object[]{fileTransferStatus.error, fileTransferStatus.transferId, fileTransferStatus.chunkIndex}));
            if (fileTransferStatus.success.booleanValue()) {
                long averageThroughput = this.fileSize / ((System.currentTimeMillis() / 1000) - this.startTime);
                showMessageAndDeleteFile(String.format(Locale.getDefault(), "File transfer %d fully completed (average speed: %s/s)", new Object[]{fileTransferStatus.transferId, FormatUtils.readableFileSize(averageThroughput)}));
            } else if (fileTransferStatus.error == FileTransferError.FILE_TRANSFER_NO_ERROR) {
                showMessageAndDeleteFile(String.format(Locale.getDefault(), "Error: Status %s returned while sending chunk %d of file transfer %d", new Object[]{fileTransferStatus.error, fileTransferStatus.chunkIndex, fileTransferStatus.transferId}));
            } else if (fileTransferStatus.chunkIndex.intValue() % 5 == 0) {
                reportCurrentTransferStatus(fileTransferStatus);
            }
        }

        private void reportCurrentTransferStatus(FileTransferStatus fileTransferStatus) {
            long sentPartSize = fileTransferStatus.totalBytesTransferred.longValue();
            long currentTime = System.currentTimeMillis() / 1000;
            long currentThroughput = (sentPartSize - this.previousThroughPutReportSize) / (currentTime - this.previousThroughPutReportTime);
            String currentETA = FormatUtils.formatDurationFromSecondsToSecondsMinutesHours((int) (((double) ((int) (currentTime - this.startTime))) * (((((double) this.fileSize) * 1.0d) / ((double) sentPartSize)) - 1.0d)));
            OTAS3BrowserFragment.this.mAppInstance.showToast(String.format(Locale.getDefault(), "Transfer %d (chunk %d):%n%s out of %s sent.%nETA: %s (current speed: %s/s)", new Object[]{fileTransferStatus.transferId, fileTransferStatus.chunkIndex, FormatUtils.readableFileSize(sentPartSize), this.fileSizeString, currentETA, FormatUtils.readableFileSize(currentThroughput)}), false, 0);
            this.previousThroughPutReportTime = currentTime;
            this.previousThroughPutReportSize = sentPartSize;
        }
    }

    protected String getS3BucketName() {
        return S3Constants.NAVDY_PROD_RELEASE_S3_BUCKET;
    }

    protected void onFileSelected(String fileS3Key) {
        final String fileName = new File(fileS3Key).getName();
        try {
            final File tempFile = File.createTempFile("fromS3_", "_" + fileName);
            try {
                storeS3ObjectInTemp(fileS3Key, tempFile, new ProgressListener() {
                    public void progressChanged(ProgressEvent progressEvent) {
                        if (progressEvent.getEventCode() == 4) {
                            new RemoteFileTransferManager(OTAS3BrowserFragment.this.mContext, tempFile, FileType.FILE_TYPE_OTA, fileName, new TempFileTransferListener(tempFile)).sendFile();
                        }
                    }
                });
            } catch (IOException e) {
                tempFile.deleteOnExit();
                sLogger.e("Exception while downloading file from S3", e);
                this.mAppInstance.showToast("Exception while downloading file from S3", false);
            }
        } catch (IOException e2) {
            sLogger.e("Exception while creating temp file", e2);
            this.mAppInstance.showToast("Exception while creating temp file", false);
        }
    }
}
