package com.navdy.proxy;

import android.os.Handler;
import android.os.Message;
import com.navdy.service.library.log.Logger;
import com.navdy.service.library.network.SocketAdapter;
import com.navdy.service.library.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ProxyThread extends Thread {
    private static final boolean VERBOSE_DBG = false;
    private static final Logger sLogger = new Logger(ProxyThread.class);
    InputStream mInputStream;
    ProxyInputThread mInputThread;
    Handler mOutputHandler;
    OutputStream mOutputStream;
    ProxyOutputThread mOutputThread;
    SocketAdapter mSocket;
    private long nativeStorage;

    private native void exitEventLoop();

    private native void receiveBytesNative(byte[] bArr, int i);

    private native void setupEventLoop();

    private native int startEventLoop();

    static {
        System.loadLibrary("proxy");
    }

    public ProxyThread(SocketAdapter socket) throws IOException {
        setName(ProxyThread.class.getSimpleName());
        this.mSocket = socket;
        this.mInputStream = socket.getInputStream();
        this.mOutputStream = socket.getOutputStream();
        sLogger.i("initialized with input " + this.mInputStream + " and output " + this.mOutputStream);
    }

    public void run() {
        super.run();
        setupEventLoop();
        this.mInputThread = new ProxyInputThread(this, this.mInputStream);
        this.mOutputThread = new ProxyOutputThread(this, this.mOutputStream);
        this.mInputThread.start();
        this.mOutputThread.start();
        this.mOutputHandler = this.mOutputThread.getHandler();
        startEventLoop();
        IOUtils.closeStream(this.mInputStream);
        IOUtils.closeStream(this.mOutputStream);
        IOUtils.closeStream(this.mSocket);
    }

    public void receiveBytes(byte[] buffer, int bytesRead) {
        receiveBytesNative(buffer, bytesRead);
    }

    private void sendOutput(byte[] buffer) {
        Message.obtain(this.mOutputHandler, 0, buffer).sendToTarget();
    }

    public void inputThreadWillFinish() {
        exitEventLoop();
    }

    public void outputThreadWillFinish() {
        exitEventLoop();
    }
}
