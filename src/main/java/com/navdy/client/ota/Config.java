package com.navdy.client.ota;

import android.content.res.Resources;
import com.alelec.navdyclient.R;

public class Config {
    long mUpdateInterval;

    public Config(Resources res) {
        this.mUpdateInterval = (long) res.getInteger(R.integer.ota_check_update_frequency);
    }
}
