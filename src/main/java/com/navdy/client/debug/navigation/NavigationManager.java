package com.navdy.client.debug.navigation;

import com.navdy.service.library.events.destination.Destination;
import com.navdy.service.library.events.destination.Destination.FavoriteType;
import com.navdy.service.library.events.location.Coordinate;
import java.util.List;

public abstract class NavigationManager {
    public abstract boolean startRouteRequest(Coordinate coordinate, String str, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2);

    public abstract boolean startRouteRequest(Coordinate coordinate, String str, List<Coordinate> list, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2);

    public abstract boolean startRouteRequest(Coordinate coordinate, String str, List<Coordinate> list, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2, String str4);

    public abstract boolean startRouteRequest(Coordinate coordinate, String str, List<Coordinate> list, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2, String str4, boolean z);

    public abstract boolean startRouteRequest(Coordinate coordinate, String str, List<Coordinate> list, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2, String str4, boolean z, boolean z2);

    public abstract boolean startRouteRequest(Coordinate coordinate, String str, List<Coordinate> list, String str2, String str3, FavoriteType favoriteType, Coordinate coordinate2, String str4, boolean z, boolean z2, Destination destination);

    public boolean startRouteRequest(Coordinate coordinate, String label, String streetAddress, Coordinate display) {
        return startRouteRequest(coordinate, label, streetAddress, null, FavoriteType.FAVORITE_NONE, display);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, Coordinate display) {
        return startRouteRequest(coordinate, label, (List) waypoints, streetAddress, null, FavoriteType.FAVORITE_NONE, display);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, Coordinate display, String requestId) {
        return startRouteRequest(coordinate, label, (List) waypoints, streetAddress, null, FavoriteType.FAVORITE_NONE, display, requestId);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, Coordinate display, String requestId, boolean useStreetAddress) {
        return startRouteRequest(coordinate, label, (List) waypoints, streetAddress, null, FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, Coordinate display, String requestId, boolean useStreetAddress, boolean initiatedOnHud) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, null, FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress, initiatedOnHud);
    }

    public boolean startRouteRequest(Coordinate coordinate, String label, List<Coordinate> waypoints, String streetAddress, Coordinate display, String requestId, boolean useStreetAddress, boolean initiatedOnHud, Destination destination) {
        return startRouteRequest(coordinate, label, waypoints, streetAddress, null, FavoriteType.FAVORITE_NONE, display, requestId, useStreetAddress, initiatedOnHud, destination);
    }
}
