package com.navdy.client.app.ui.firstlaunch;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.alelec.navdyclient.R;
import com.navdy.client.app.tracking.Tracker;
import com.navdy.client.app.tracking.TrackerConstants.Screen.FirstLaunch.Install;
import com.navdy.client.app.ui.base.BaseToolbarActivity;
import com.navdy.client.app.ui.base.BaseToolbarActivity.ToolbarBuilder;
import com.navdy.client.app.ui.settings.SettingsConstants;
import com.navdy.client.app.ui.settings.SettingsUtils;

public class BoxPickerActivity extends BaseToolbarActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fle_box_picker);
        new ToolbarBuilder().title((int) R.string.select_box).build();
        loadImage(R.id.illustration, R.drawable.image_navdy_select);
        loadImage(R.id.new_box_illustration, R.drawable.image_navdy_sm);
        loadImage(R.id.new_box_plus_mounts_illustration, R.drawable.image_navdy_plus_sm);
        loadImage(R.id.old_box_illustration, R.drawable.image_navdy_oversized_sm);
    }

    protected void onResume() {
        super.onResume();
        hideSystemUI();
        Tracker.tagScreen(Install.BOX_PICKER);
    }

    public void onNewBoxClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.BOX, "New_Box").apply();
        Intent intent = new Intent(getApplicationContext(), BoxViewerActivity.class);
        intent.putExtra(BoxViewerActivity.EXTRA_BOX, "New_Box");
        startActivity(intent);
    }

    public void onNewBoxPlusMountsClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.BOX, "New_Box_Plus_Mounts").apply();
        Intent intent = new Intent(getApplicationContext(), BoxViewerActivity.class);
        intent.putExtra(BoxViewerActivity.EXTRA_BOX, "New_Box_Plus_Mounts");
        startActivity(intent);
    }

    public void onOldBoxClick(View view) {
        SettingsUtils.getSharedPreferences().edit().putString(SettingsConstants.BOX, "Old_Box").apply();
        Intent intent = new Intent(getApplicationContext(), BoxViewerActivity.class);
        intent.putExtra(BoxViewerActivity.EXTRA_BOX, "Old_Box");
        startActivity(intent);
    }

    public void onBackClick(View view) {
        finish();
    }
}
