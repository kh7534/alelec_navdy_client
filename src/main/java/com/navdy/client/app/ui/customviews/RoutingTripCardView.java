package com.navdy.client.app.ui.customviews;

import android.content.Context;
import android.util.AttributeSet;
import com.navdy.client.app.framework.models.Destination;
import com.navdy.client.app.framework.models.Destination.MultilineAddress;
import com.navdy.service.library.log.Logger;

public class RoutingTripCardView extends TripCardView {
    private static final Logger logger = new Logger(RoutingTripCardView.class);

    public RoutingTripCardView(Context context) {
        this(context, null, 0);
    }

    public RoutingTripCardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoutingTripCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    protected String appendPrefix(String name) {
        return name;
    }

    public void setNameAddressAndIcon(Destination destination) {
        if (destination == null) {
            logger.d("Destination object is null");
            return;
        }
        if (this.icon != null) {
            this.icon.setImageResource(destination.getBadgeAssetForActiveTrip());
        }
        MultilineAddress mla = destination.getMultilineAddress();
        if (this.tripName != null) {
            this.tripName.setText(appendPrefix(mla.title));
        }
        if (this.tripAddressFirstLine != null) {
            this.tripAddressFirstLine.setText(mla.addressFirstLine);
        }
        if (this.tripAddressSecondLine != null) {
            this.tripAddressSecondLine.setText(mla.addressSecondLine);
        }
    }
}
